import * as actions from "./action-types";

export function cmdLogin() {
  return {
    type: actions.CMD_LOGIN
  };
}

export function cmdLogout() {
  return {
    type: actions.CMD_LOGOUT
  };
}
