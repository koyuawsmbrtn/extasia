import storage from 'redux-persist/lib/storage';

const initialState = {
  // base
  loggedIn: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'CMD_LOGIN':
      return {
          ...state,
          loggedIn: true
        }
    case 'CMD_LOGOUT':
      return {
        ...state,
        loggedIn: false
      }
    case 'HARD_RESET':
      storage.removeItem('persist:root')
      state = undefined
      break;
    default:
      return state;
  }
};
