import React from 'react';
import $ from 'jquery';
import * as PIXI from 'pixi.js';
import './Client.css';
import './Login.css';
import { connect } from "react-redux";
import { cmdLogin, cmdLogout } from "../reducer/actions";

// Rooms
import empty_room from '../assets/rooms/empty.png';
// eslint-disable-next-line
import little_room from '../assets/rooms/little_room.png';

// Characters
import empty_character_texture from '../assets/entities/empty.png';
var texture = PIXI.Texture.from(empty_character_texture);
texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
var empty_character = new PIXI.Sprite(texture);

// Resize character
empty_character.width = empty_character.width / 4;
empty_character.height = empty_character.height / 4;

// Room
var texture = PIXI.Texture.from(empty_room);
texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
var room = new PIXI.Sprite(texture);

// Create room
var roomConfig = {
  w: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
  h: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
}
var app = new PIXI.Application({ backgroundColor: 0x000000, width: roomConfig.w, height: roomConfig.h });

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = { cmdLogin, cmdLogout };

class Canvas extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount () {
    document.getElementById("canvas").appendChild(app.view);
    var componentRef = this;

    createRoom();

    // Define client functions
    function createRoom() {
        var x = Math.floor(app.screen.width / 2);
        var y = Math.floor(app.screen.height / 2);

        // enable the room to be interactive... this will allow it to respond to mouse and touch events
        room.interactive = true;

        // this button mode will mean the hand cursor appears when you roll over the room with your mouse
        room.buttonMode = true;

        // center the room's anchor point
        room.anchor.set(0.5, 0.5);

        // setup events for mouse + touch using
        // the pointer events
        room
            .on('pointerdown', onDragStart)
            .on('pointerup', onDragEnd)
            .on('pointerupoutside', onDragEnd)
            .on('pointermove', onDragMove);

        // For mouse-only events
        // .on('mousedown', onDragStart)
        // .on('mouseup', onDragEnd)
        // .on('mouseupoutside', onDragEnd)
        // .on('mousemove', onDragMove);

        // For touch-only events
        // .on('touchstart', onDragStart)
        // .on('touchend', onDragEnd)
        // .on('touchendoutside', onDragEnd)
        // .on('touchmove', onDragMove);

        // move the sprite to its designated position
        room.x = x;
        room.y = y;

        // Position the character
        empty_character.x = room.x;
        empty_character.y = room.y;

        // add it to the stage
        app.stage.addChild(room);
        app.stage.addChild(empty_character);
    }

    function onDragStart(event) {
        // store a reference to the data
        // the reason for this is because of multitouch
        // we want to track the movement of this particular touch
        this.data = event.data;
        this.dragging = true;
    }

    function onDragEnd() {
        this.dragging = false;
        // set the interaction data to null
        this.data = null;
    }

    function onDragMove(room) {
        if (this.dragging) {
          const newPosition = this.data.getLocalPosition(this.parent);
          this.x = newPosition.x;
          this.y = newPosition.y;
          empty_character.x = newPosition.x;
          empty_character.y = newPosition.y;
        }
    }

    $("#chat").on("keydown", function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      if(code === 13) {
          $("#chat").val("");
      }
    });
  }

  render() {
    return(
      <React.Fragment>
        <div id="canvas"></div>
      </React.Fragment>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Canvas);
