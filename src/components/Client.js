import React from 'react';
import './Client.css';
import './Login.css';
// Hotel background
import Hotel from '../assets/hotel.png';
import Login from './Login';
import Notice from './template/Notice'
import TopBar from './template/TopBar'
import Footer from './template/Footer'
import { connect } from "react-redux";

import Canvas from './Canvas'

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

class Client extends React.Component {
  render() {

    let mainStyle = {
      background: `url(${Hotel}) no-repeat top center`,
      width: "100%",
      height: "100%",
      margin: 0,
      padding: 0
    }

    let loggedIn = this.props.loggedIn;

    return(
      <div id="main" style={loggedIn ? {} : mainStyle}>

      <TopBar />

        {loggedIn ?
          <React.Fragment>
            <Canvas />
            <Footer />
          </React.Fragment> :
          <React.Fragment>
            <Login />
            <Notice />
          </React.Fragment>
        }

      </div>
    )
  }
}

export default connect(
  mapStateToProps
)(Client);
