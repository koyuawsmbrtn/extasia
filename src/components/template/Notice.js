import React from 'react';

export default class Notice extends React.Component {
  render() {
    return(
        <main role="main" className="flex-shrink-0">
          <div className="container notice">
            <p>This is an early test build and may not function properly or at all.</p>
            <p>You can contribute over on <a href="https://git.koyu.space/koyu/extasia" target="_blank" rel="noopener noreferrer">https://git.koyu.space/koyu/extasia</a></p>
            <p>if you want to help.</p>
            <br />
            <p>This software is not affiliated in any way with Habbo or Sulake Oy.<br />Habbo is a registered trademark of Sulake Oy.<br />All rights reserved to their respective owners.</p>
          </div>
        </main>
    )
  }
}
