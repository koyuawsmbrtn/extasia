import React from 'react';
import E from '../../assets/font/e.png';

export default class Footer extends React.Component {
  render() {
    return(
      <footer className="footer mt-auto py-3">
          <span className="left_section">
            <img src={E} alt="E" className="font" height="37" />
            <button><img src="/icons/rooms.png" alt="Rooms" className="icon" /></button>
            <button><img src="/icons/shop.png" alt="Shop" className="icon" height="37" /></button>
            <button><img src="/icons/inventory.png" alt="Inventory" className="icon" height="37" /></button>
          </span>
          <span className="middle_section">
            <button><img src="/icons/ghosthead.png" alt="Me" /></button>
            <input type="text" maxLength="95" name="chat" id="chat" autoComplete="off" placeholder="Begin a chat..." /><button><img src="/icons/chat_styles.png" alt="Chat styles" /></button>
          </span>
          <span className="right_section">
            <button><img src="/icons/all_friends.png" alt="Friends" /></button><button><img src="/icons/messenger.png" alt="Messenger" /></button>
          </span>
      </footer>
    )
  }
}
