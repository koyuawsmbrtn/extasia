import React from 'react';
// Minicons
import minicon_logout from '../../assets/minicons/logout.png';
import minicon_settings from '../../assets/minicons/settings.png';
import { connect } from "react-redux";
import { cmdLogin, cmdLogout } from "../../reducer/actions";

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = { cmdLogin, cmdLogout };

class TopBar extends React.Component {
  render() {
    return(
      <div className="top-bar">
        <button id="settings" className="bar_content"><img src={minicon_settings} alt="Settings" /></button>
        <button id="logout" onClick={this.props.cmdLogout} className="bar_content"><img src={minicon_logout} alt="Logout" /></button>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TopBar)
