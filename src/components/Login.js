import React from 'react';
import './Login.css';
import { connect } from "react-redux";
import { cmdLogin, cmdLogout } from "../reducer/actions";

const mapStateToProps = state => {
  return {
    loggedIn: state.loggedIn
  };
};

const mapDispatchToProps = { cmdLogin, cmdLogout };

// TODO: Login function
class Login extends React.Component {
    render() {
        return(
            <div id="loginform">
                {/* <img src="/logo.png" alt="Logo" className="logo" /> */}
                {/* Will be uncommented when a new logo has been made  */}
                <div className="dialog">
                    <h2 className="title">Login</h2>
                        <div className="wrapper">
                            <p>Login with your fediverse account to start playing.</p>
                            <label>Username:</label> <input type="text" name="username" id="username" />
                            <label>Password:</label> <input type="password" name="password" id="password" />
                            <label>Instance:</label> <input type="text" name="instance" id="instance" placeholder="e.g. koyu.space" />
                            <button id="login" onClick={this.props.cmdLogin}>Login</button>
                            <br />
                        </div>
                </div>
            </div>
        )
    }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
